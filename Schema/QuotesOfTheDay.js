const mongoose = require('mongoose')

const quoteData = mongoose.Schema({
    id: String,
    quote: String,
    author: String
})

const quotesOfTheDaySchema = mongoose.Schema({
    quote_of_the_day: quoteData,
    love_quote_of_the_day: quoteData,
    funny_quote_of_the_day: quoteData,
    art_quote_of_the_day: quoteData,
    nature_quote_of_the_day: quoteData,
}, { timestamps: true })

module.exports = mongoose.model("quotesoftheday", quotesOfTheDaySchema)