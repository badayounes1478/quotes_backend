const mongoose = require('mongoose')

const groupNamesSchema = mongoose.Schema({
    organizationId: String,
    groupName: [String]
})

module.exports = mongoose.model("GroupName", groupNamesSchema)



