const mongoose = require('mongoose')

const quoteSchema = mongoose.Schema({
    Queote: String,
    Author: String,
    Tags: [String],
})

// search field implementation
quoteSchema.index({
    Queote: "text",
    Author: "text",
    Tags: "text"
},{
    name: "TextIndex",
    weights: {
        Author: 8,
        Tags: 5,
        Queote: 10
    }
})


module.exports = mongoose.model("quote", quoteSchema)

