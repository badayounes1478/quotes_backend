const mongoose = require('mongoose')

const authorSchema = mongoose.Schema({
    authorName: {
        type: String,
        unique: true,
        trim: true,
        lowercase: true
    }
})

module.exports = mongoose.model("author", authorSchema)