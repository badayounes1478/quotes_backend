const { createLogger, format, transports } = require("winston");
const { combine, timestamp, printf } = format;

const myFormat = printf(({ level, message, label, timestamp }) => {
  return `${timestamp} ${level}: ${message}`;
});

const developementLogger = () => {
  return createLogger({
    format: combine(timestamp(), myFormat),
    transports: [new transports.Console()],
  });
};

module.exports = developementLogger;
