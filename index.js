const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const { scheduleQuoteOfTheday } = require('./Controllers/Quotes')
const logger = require("./logger")
const app = express()

mongoose.connect('mongodb+srv://creatosaurus1:EOaVFfQ5YhOD3UhF@creatosaurus.7trc5.mongodb.net/Quotes', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
}).then(() => {
    scheduleQuoteOfTheday()
    logger.info('Quotes database connection success');
}).catch((error) => {
    logger.error(error)
    process.exit(1);
})

app.use(cors())
app.use(express.json())
app.use('/quotes', require('./Routes/Quotes'))

module.exports = app;
