const express = require('express');
const {
  getQuotes,
  createQuotes,
  searchQuotes,
  getGroupsById,
  createGroups,
  getSavedQuotes,
  saveQuote,
  deleteQuote,
  getPopularQuotes,
  getQuotesOfTheDay,
  addQuotesOfTheDay,
  getHundredQuotes,
  getSingleQuote
} = require('../Controllers/Quotes');
const router = express.Router();
const quotes = require('../Schema/QuotesSchema');
const fs = require('fs');

// get quotes
router.get('/', getQuotes);

// create quotes
router.post('/create', createQuotes);

// search quotes
router.get('/search', searchQuotes);

// get single quote
router.get("/get/single/quote", getSingleQuote)

// get groups
router.get('/groups/:id', getGroupsById);

// create group
router.post('/group/create', createGroups);

// get saved quotes
router.get('/saved/:id', getSavedQuotes);

// save quotes
router.post('/save', saveQuote);

// TODO: bug not deleting saved quotes by organization
// delete quotes
router.delete('/save/remove/:id/:userId', deleteQuote);

// get popular quotes
router.get('/popular', getPopularQuotes);

// get quotes of the day
router.get('/quotesoftheday', getQuotesOfTheDay);

// add quotes of the day
router.post('/add/quotesoftheday', addQuotesOfTheDay);

// get 100 quotes
router.get('/top100quotes', getHundredQuotes);

// const slugify = (string) => {
//   return string
//     .toString()
//     .trim()
//     .toLowerCase()
//     .replace(/\s+/g, '-')
//     .replace(/[^\w\-]+/g, '')
//     .replace(/\-\-+/g, '-')
//     .replace(/^-+/, '')
//     .replace(/-+$/, '');
// };

// generating xml 824412 don't delete
// router.get('/generatexml', async (req, res) => {
//   try {
//     const limit = 10000;
//     for (let index = 0; index < 85; index++) {
//       const skip = index + 1;

//       const data = await quotes.find().skip(skip).limit(limit);
//       if (data.length === 0) continue;

//       for (let i = 0; i < data.length; i++) {
//         let fiveWordsSlug = data[i].Queote.split(' ').slice(0, 5).join(' ');
//         let slug;
//         if (fiveWordsSlug !== undefined) {
//           slug = slugify(fiveWordsSlug);
//         }
//         let trimedAuthor;
//         if (data[i].Author !== undefined) {
//           if (data[i].Author.indexOf(',') > -1) {
//             trimedAuthor = data[i].Author.substring(
//               0,
//               data[i].Author.indexOf(',')
//             );
//           } else {
//             trimedAuthor = data[i].Author;
//           }
//         }
//         let slugAuthor;
//         if (trimedAuthor !== undefined) {
//           slugAuthor = slugify(trimedAuthor);
//         }
//         let author;
//         if (slugAuthor !== undefined) {
//           author = slugAuthor.substring(0, 30);
//         }

//         let xmlStructure = `
// <url>
// <loc>https://www.creatosaurus.io/apps/quotes/${slug}-quote-by-${author}-id-${data[i]._id}</loc>
// <lastmod>${new Date().toISOString()}</lastmod>
// <changefreq>daily</changefreq>
// <priority>0.8</priority>
// </url>`;

//         fs.appendFileSync(`./Xml/${index}.xml`, xmlStructure);
//       }
//     }

//     res.send({ msg: 'done' });
//   } catch (error) {
//     console.log(error);
//   }
// });

// topicwise sitemap generation
// router.get('/generatexml', async (req, res) => {
//   try {
//     const limit = 10000;
//     let topicArray = [];
//     for (let index = 0; index < 85; index++) {
//       const skip = index + 1;
//       const data = await quotes
//         .find({}, { Tags: 1, _id: 0 })
//         .skip(skip)
//         .limit(limit);
//       data.forEach((tag) => {
//         tag.Tags.forEach((singleTag) => {
//           let arrayTags = singleTag.split(',');
//           arrayTags.forEach((data) => {
//             topicArray.push(data.trim().toLowerCase());
//           });
//         });
//       });
//     }
//     let uniqueTopic = [...new Set(topicArray)];
//     uniqueTopic.forEach((tag, i) => {
//       let xmlStructure = `
// <url>
// <loc>https://www.creatosaurus.io/apps/quotes/topics/${tag}-quotes</loc>
// <lastmod>${new Date().toISOString()}</lastmod>
// <changefreq>daily</changefreq>
// <priority>1.0</priority>
// </url>`;
//       fs.appendFileSync(`./Xml/topic.xml`, xmlStructure);
//     });
//     res.send('done');
//   } catch (error) {
//     console.log(error);
//   }
// });

// authorwise sitemap generation
// router.get('/generatexml', async (req, res) => {
//   try {
//     const limit = 10000;
//     let authorArray = [];

//     for (let index = 0; index < 85; index++) {
//       const skip = index + 1;
//       const data = await quotes
//         .find({}, { Author: 1, _id: 0 })
//         .skip(skip)
//         .limit(limit);
//       data.forEach((author) => {
//         let trimedAuthor;
//         if (author.Author !== undefined) {
//           if (author.Author.indexOf(',') > -1) {
//             trimedAuthor = author.Author.substring(
//               0,
//               author.Author.indexOf(',')
//             );
//           } else {
//             trimedAuthor = author.Author;
//           }
//         }
//         let slugAuthor;
//         if (trimedAuthor !== undefined) {
//           slugAuthor = slugify(trimedAuthor);
//         }
//         let authorname;
//         if (slugAuthor !== undefined) {
//           authorname = slugAuthor.substring(0, 30);
//         }

//         authorArray.push(authorname);
//       });
//     }
//     let uniqueAuthor = [...new Set(authorArray)];
//     uniqueAuthor.forEach((author, i) => {
//       let xmlStructure = `
// <url>
// <loc>https://www.creatosaurus.io/apps/quotes/authors/${author}-quotes</loc>
// <lastmod>${new Date().toISOString()}</lastmod>
// <changefreq>daily</changefreq>
// <priority>1.0</priority>
// </url>
//       `;

//       fs.appendFileSync(`./authorXml/author.xml`, xmlStructure);
//     });
//     res.send('done');
//   } catch (error) {
//     console.log(error);
//   }
// });

module.exports = router;
