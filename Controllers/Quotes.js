const groupes = require('../Schema/GroupNameSchema');
const quotes = require('../Schema/QuotesSchema');
const savedQuotes = require('../Schema/SaveQuotesSchema');
const quotesOfTheDay = require('../Schema/QuotesOfTheDay');
const schedule = require('node-schedule');
const logger = require('../logger');

exports.getQuotes = async (req, res) => {
  try {
    const limit = parseInt(req.query.limit) || 10;
    const skip = limit * (parseInt(req.query.page) - 1) || 0;

    const data = await quotes.find().skip(skip).limit(limit);
    res.send(data);
  } catch (error) {
    logger.error(error);
    res.status(500).json({
      message: 'Internal server error',
    });
  }
};

exports.createQuotes = async (req, res) => {
  try {
    // check same quote is present in database or not
    let data = await quotes.find({
      $text: {
        $search: `\"${req.body.Queote}\"`,
      },
    });

    if (data.length !== 0) {
      res.status(400).json({
        error: 'quote all ready present in database',
      });
    } else {
      // create quote
      let data = await quotes.create(req.body);
      res.status(200).json({
        data: data,
      });
    }
  } catch (error) {
    logger.error(error);
    res.status(500).json({
      message: 'Internal server error',
    });
  }
};

exports.searchQuotes = async (req, res) => {
  try {
    const limit = parseInt(req.query.limit) || 10;
    const skip = limit * (parseInt(req.query.page) - 1) || 0;

    if (req.query.q === 'null') {
      let data = await quotes.find({}).skip(skip).limit(limit);
      res.send(data);
    } else {
      let data = await quotes
        .find({
          $text: {
            $search: `\"${req.query.q}\"`,
          },
        })
        .skip(skip)
        .limit(limit);

      //if noting found with exact match found with character match
      if (data.length === 0) {
        data = await quotes
          .find({
            $text: {
              $search: req.query.q,
            },
          })
          .skip(skip)
          .limit(limit);
      }
      res.send(data);
    }
  } catch (error) {
    logger.error(error);
    return res.status(500).json({
      message: 'Internal server error',
    });
  }
};

exports.createGroups = async (req, res) => {
  try {
    const data = await groupes.findOne({
      organizationId: req.body.organizationId,
    });
    if (data === null) {
      await groupes.create(req.body);
      res.status(201).json({
        message: 'created',
      });
    } else {
      data.groupName = [...data.groupName, req.body.groupName];
      await data.save();
      res.status(200).json({
        message: 'updated',
      });
    }
  } catch (error) {
    logger.error(error);
    return res.status(500).json({
      message: 'Internal server error',
    });
  }
};

exports.getGroupsById = async (req, res) => {
  try {
    const data = await groupes.findOne({ organizationId: req.params.id });
    res.send(data);
  } catch (error) {
    logger.error(error);
    return res.status(500).json({
      message: 'Internal server error',
    });
  }
};

exports.saveQuote = async (req, res) => {
  try {
    const allReadyExistes = await savedQuotes.findOne({
      organizationId: req.body.organizationId,
      quote: req.body.quote,
      groupName: req.body.groupName,
    });
    if (allReadyExistes !== null)
      return res.status(409).json({ error: 'Quote already saved' });
    const data = await savedQuotes.create(req.body);
    res.status(201).json({
      message: 'created',
      data: data,
    });
  } catch (error) {
    logger.error(error);
    return res.status(500).json({
      message: 'Internal server error',
    });
  }
};

exports.getSavedQuotes = async (req, res) => {
  try {
    const limit = parseInt(req.query.limit) || 10;
    const skip = limit * (parseInt(req.query.page) - 1) || 0;

    const data = await savedQuotes
      .find({ organizationId: req.params.id })
      .sort({ $natural: -1 })
      .skip(skip).
      limit(limit);

    res.send(data);
  } catch (error) {
    logger.error(error);
    return res.status(500).json({
      message: 'Internal server error',
    });
  }
};

exports.deleteQuote = async (req, res) => {
  try {
    await savedQuotes.deleteOne({
      _id: req.params.id,
      organizationId: req.params.userId,
    });
    res.status(200).json({
      message: 'deleted',
    });
  } catch (error) {
    logger.error(error);
    return res.status(500).json({
      message: 'Internal server error',
    });
  }
};

exports.getPopularQuotes = async (req, res) => {
  try {
    const limit = 6;
    const skip = Math.floor(Math.random() * 300 + 100);
    const data = await quotes.find().skip(skip).limit(limit);
    res.send(data);
  } catch (error) {
    logger.error(error);
    return res.status(500).json({
      message: 'Internal server error',
    });
  }
};

exports.scheduleQuoteOfTheday = () => {
  schedule.scheduleJob('0 0 * * *', async () => {
    try {
      const quote = await quotesOfTheDay.find();
      if (quote.length >= 1) {
        await quotesOfTheDay.remove({});
        const quoteOfTheDay = await quotes.findOne();
        const love = await quotes.findOne({ Tags: 'Love' });
        const art = await quotes.findOne({ Tags: 'Art' });
        const nature = await quotes.findOne({ Tags: 'nature' });
        const funny = await quotes.findOne({ Tags: 'funny' });

        const body = {
          quote_of_the_day: {
            id: quoteOfTheDay._id,
            quote: quoteOfTheDay.Queote,
            author: quoteOfTheDay.Author,
          },
          love_quote_of_the_day: {
            id: love._id,
            quote: love.Queote,
            author: love.Author,
          },
          funny_quote_of_the_day: {
            id: funny._id,
            quote: funny.Queote,
            author: funny.Author,
          },
          art_quote_of_the_day: {
            id: art._id,
            quote: art.Queote,
            author: art.Author,
          },
          nature_quote_of_the_day: {
            id: nature._id,
            quote: nature.Queote,
            author: nature.Author,
          },
        };

        const data = await quotesOfTheDay.create(body);
      } else {
        const quoteOfTheDay = await quotes.findOne();
        const love = await quotes.findOne({ Tags: 'Love' });
        const art = await quotes.findOne({ Tags: 'Art' });
        const nature = await quotes.findOne({ Tags: 'nature' });
        const funny = await quotes.findOne({ Tags: 'funny' });

        const body = {
          quote_of_the_day: {
            id: quoteOfTheDay._id,
            quote: quoteOfTheDay.Queote,
            author: quoteOfTheDay.Author,
          },
          love_quote_of_the_day: {
            id: love._id,
            quote: love.Queote,
            author: love.Author,
          },
          funny_quote_of_the_day: {
            id: funny._id,
            quote: funny.Queote,
            author: funny.Author,
          },
          art_quote_of_the_day: {
            id: art._id,
            quote: art.Queote,
            author: art.Author,
          },
          nature_quote_of_the_day: {
            id: nature._id,
            quote: nature.Queote,
            author: nature.Author,
          },
        };
        const data = await quotesOfTheDay.create(body);
      }
    } catch (error) {
      logger.error(error);
      return res.status(500).json({
        message: 'Internal server error',
      });
    }
  });
};

exports.addQuotesOfTheDay = async (req, res) => {
  try {
    const quote = await quotesOfTheDay.find();
    if (quote.length >= 1) {
      await quotesOfTheDay.remove({});
      const quoteOfTheDay = await quotes.findOne();
      const love = await quotes.findOne({ Tags: 'Love' });
      const art = await quotes.findOne({ Tags: 'Art' });
      const nature = await quotes.findOne({ Tags: 'nature' });
      const funny = await quotes.findOne({ Tags: 'funny' });

      const body = {
        quote_of_the_day: {
          id: quoteOfTheDay._id,
          quote: quoteOfTheDay.Queote,
          author: quoteOfTheDay.Author,
        },
        love_quote_of_the_day: {
          id: love._id,
          quote: love.Queote,
          author: love.Author,
        },
        funny_quote_of_the_day: {
          id: funny._id,
          quote: funny.Queote,
          author: funny.Author,
        },
        art_quote_of_the_day: {
          id: art._id,
          quote: art.Queote,
          author: art.Author,
        },
        nature_quote_of_the_day: {
          id: nature._id,
          quote: nature.Queote,
          author: nature.Author,
        },
      };

      const data = await quotesOfTheDay.create(body);
      res.status(200).json({
        message: 'created',
        data: data,
      });
    } else {
      const quoteOfTheDay = await quotes.findOne();
      const love = await quotes.findOne({ Tags: 'Love' });
      const art = await quotes.findOne({ Tags: 'Art' });
      const nature = await quotes.findOne({ Tags: 'nature' });
      const funny = await quotes.findOne({ Tags: 'funny' });

      const body = {
        quote_of_the_day: {
          id: quoteOfTheDay._id,
          quote: quoteOfTheDay.Queote,
          author: quoteOfTheDay.Author,
        },
        love_quote_of_the_day: {
          id: love._id,
          quote: love.Queote,
          author: love.Author,
        },
        funny_quote_of_the_day: {
          id: funny._id,
          quote: funny.Queote,
          author: funny.Author,
        },
        art_quote_of_the_day: {
          id: art._id,
          quote: art.Queote,
          author: art.Author,
        },
        nature_quote_of_the_day: {
          id: nature._id,
          quote: nature.Queote,
          author: nature.Author,
        },
      };
      const data = await quotesOfTheDay.create(body);
      res.status(200).json({
        message: 'created',
        data: data,
      });
    }
  } catch (error) {
    logger.error(error);
    return res.status(500).json({
      message: 'Internal server error',
    });
  }
};

exports.getQuotesOfTheDay = async (req, res) => {
  try {
    const data = await quotesOfTheDay.find();
    res.status(200).json(data);
  } catch (error) {
    logger.error(error);
    return res.status(500).json({
      message: 'Internal server error',
    });
  }
};

exports.getHundredQuotes = async (req, res) => {
  try {
    const limit = 100;
    const skip = Math.floor(Math.random() * 300 + 100);
    const data = await quotes.find().skip(skip).limit(limit);
    res.send(data);
  } catch (error) {
    logger.error(error);
    return res.status(500).json({
      message: 'Internal server error',
    });
  }
};

exports.getSingleQuote = async (req, res) => {
  try {
    const data = await quotes.findOne({ _id: req.query.search })
    res.status(200).json(data)
  } catch (error) {
    logger.error(error);
    return res.status(500).json({
      message: 'Internal server error',
    });
  }
}
